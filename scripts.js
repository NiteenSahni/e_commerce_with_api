function getId(id) {
    removeDivs()
    let ClothesGrid = document.createElement('div')
    let JwelleryGrid = document.createElement('div')
    let EclectronicsGrid = document.createElement('div')
    let OtherGrid = document.createElement('div')
    userInput = document.getElementById('limitResults').value
    console.log(userInput)
    fetch(`https://fakestoreapi.com/products/${id}`).then((response) => {
        return response.json()
    }).then((data) => {
        // data = Array.from(data)
        console.log(data)

        if (data.category.includes(`clothing`)) {

            const productDiv = document.createElement('div')
            productDiv.innerHTML = `<div class='imageBox'><img class="productImages" src="${data.image}"/></div>
            <p class="productName">${data.title}</p>
            <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${data.rating['rate']}</button>
            <p class="productPrice">Price - ${data.price} $</p>
            <p class="productDescription">${data.description}</p>
            <button class="addToCart">Add To Cart</button>`
            productDiv.setAttribute('class', "miProducts")

            ClothesGrid.setAttribute('class', 'productsGrid')
            ClothesGrid.setAttribute('id', 'ClothesGrid')
            productDiv.setAttribute('class', "miProducts")
            let infoDiv = document.createElement('div')
            infoDiv.setAttribute('class', 'description')
            infoDiv.setAttribute('id', 'ClothesGrid')
            infoDiv.innerHTML = `About this item :- <p class="descriptionBox">${data.description}<button class="descriptionButton">Add to Cart</button> </p>`
            document.getElementById("Clothing").appendChild(infoDiv);
            document.getElementById("Clothing").appendChild(ClothesGrid);
            document.getElementById("ClothesGrid").appendChild(productDiv);

        }

        if (data.category === `jewelery`) {

            const productDiv = document.createElement('div')
            productDiv.innerHTML = `<div class='imageBox'><img class="productImages" src="${data.image}"/></div>
            <p class="productName">${data.title}</p>
            <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${data.rating['rate']}</button>
            <p class="productPrice">Price - ${data.price} $</p>
            <p class="productDescription">${data.description}</p>
            <button class="addToCart">Add To Cart</button>`
            productDiv.setAttribute('class', "miProducts")

            JwelleryGrid.setAttribute('class', 'productsGrid')
            JwelleryGrid.setAttribute('id', 'JwelleryGrid')
            productDiv.setAttribute('class', "miProducts")
            let infoDiv = document.createElement('div')
            infoDiv.setAttribute('class', 'description')
            infoDiv.setAttribute('id', 'JwelleryGrid')
            infoDiv.innerHTML = `About this item :- <p class="descriptionBox">${data.description}<button class="descriptionButton">Add to Cart</button> </p>`
            document.getElementById("Jwellery").appendChild(infoDiv);
            document.getElementById("Jwellery").appendChild(JwelleryGrid);
            document.getElementById("JwelleryGrid").appendChild(productDiv);

        }

        if (data.category === `electronics`) {

            const productDiv = document.createElement('div')
            productDiv.innerHTML = `<div class='imageBox'><img class="productImages" src="${data.image}"/></div>
            <p class="productName">${data.title}</p>
            <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${data.rating['rate']}</button>
            <p class="productPrice">Price - ${data.price} $</p>
            <p class="productDescription">${data.description}</p>
            <button class="addToCart">Add To Cart</button>`
            productDiv.setAttribute('class', "miProducts")

            EclectronicsGrid.setAttribute('class', 'productsGrid')
            EclectronicsGrid.setAttribute('id', 'EclectronicsGrid')
            productDiv.setAttribute('class', "miProducts")
            let infoDiv = document.createElement('div')
            infoDiv.setAttribute('class', 'description')
            infoDiv.setAttribute('id', 'EclectronicsGrid')
            infoDiv.innerHTML = `About this item :- <p class="descriptionBox">${data.description}<button class="descriptionButton">Add to Cart</button> </p>`
            document.getElementById("Electronics").appendChild(infoDiv);
            document.getElementById("Electronics").appendChild(EclectronicsGrid);
            document.getElementById("EclectronicsGrid").appendChild(productDiv);

        }


    })


}
fetch('https://fakestoreapi.com/products').then((response) => {
    return response.json()
}).then((data) => {
    let ClothesGrid = document.createElement('div')
    let JwelleryGrid = document.createElement('div')
    let EclectronicsGrid = document.createElement('div')
    let OtherGrid = document.createElement('div')
    let productData = data
    data.forEach((indiData) => {
        if (indiData.category.includes(`clothing`)) {

            const productDiv = document.createElement('div')
            productDiv.innerHTML = `<div class='imageBox'><img onclick='getId(${indiData.id})' class="productImages" src="${indiData.image}"/></div>
        <p class="productName">${indiData.title}</p>
        <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${indiData.rating['rate']}</button>
        <p class="productPrice">Price - ${indiData.price} $</p>
        <p class="productDescription">${indiData.description}</p>
        <button class="addToCart">Add To Cart</button>`
            productDiv.setAttribute('class', "miProducts")

            ClothesGrid.setAttribute('class', 'productsGrid')
            ClothesGrid.setAttribute('id', 'ClothesGrid')
            productDiv.setAttribute('class', "miProducts")
            document.getElementById("Clothing").appendChild(ClothesGrid);
            document.getElementById("ClothesGrid").appendChild(productDiv);
        }

        if (indiData.category === `jewelery`) {

            const productDiv = document.createElement('div')
            productDiv.innerHTML = `<div class='imageBox'><img onclick='getId(${indiData.id})' class="productImages" src="${indiData.image}"/></div>
        <p class="productName">${indiData.title}</p>
        <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${indiData.rating['rate']}</button>
        <p class="productPrice">Price - ${indiData.price} $</p>
        <p class="productDescription">${indiData.description}</p>
        <button class="addToCart">Add To Cart</button>`
            productDiv.setAttribute('class', "miProducts")

            JwelleryGrid.setAttribute('class', 'productsGrid')
            JwelleryGrid.setAttribute('id', 'JwelleryGrid')
            productDiv.setAttribute('class', "miProducts")
            document.getElementById("Jwellery").appendChild(JwelleryGrid);
            document.getElementById("JwelleryGrid").appendChild(productDiv);
        }

        if (indiData.category === `electronics`) {

            const productDiv = document.createElement('div')
            productDiv.innerHTML = `<div class='imageBox'><img onclick='getId(${indiData.id})' class="productImages" src="${indiData.image}"/></div>
        <p class="productName">${indiData.title}</p>
        <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${indiData.rating['rate']}</button>
        <p class="productPrice">Price - ${indiData.price} $</p>
        <p class="productDescription">${indiData.description}</p>
        <button class="addToCart">Add To Cart</button>`
            productDiv.setAttribute('class', "miProducts")

            EclectronicsGrid.setAttribute('class', 'productsGrid')
            EclectronicsGrid.setAttribute('id', 'EclectronicsGrid')
            productDiv.setAttribute('class', "miProducts")
            document.getElementById("Electronics").appendChild(EclectronicsGrid);
            document.getElementById("EclectronicsGrid").appendChild(productDiv);
        }


    })

})

function removeDivs() {

    let clothesList = document.getElementById('Clothing')
    let jeweleryList = document.getElementById('Jwellery')
    let electronicsList = document.getElementById('Electronics')
    for (let index = 0; index = clothesList.children.length; index++) {
        if (clothesList.hasChildNodes()) {
            clothesList.removeChild(clothesList.children[0]);

        }
    }
    for (let index = 0; index <= jeweleryList.children.length; index++) {
        if (jeweleryList.hasChildNodes()) {
            jeweleryList.removeChild(jeweleryList.children[0]);

        }
    }
    for (let index = 0; index <= electronicsList.children.length; index++) {
        if (electronicsList.hasChildNodes()) {
            electronicsList.removeChild(electronicsList.children[0]);

        }
    }
    console.log("asda")
}

function limitResults() {
    removeDivs()
    let ClothesGrid = document.createElement('div')
    let JwelleryGrid = document.createElement('div')
    let EclectronicsGrid = document.createElement('div')
    let OtherGrid = document.createElement('div')
    userInput = document.getElementById('limitResults').value
    console.log(userInput)
    fetch(`https://fakestoreapi.com/products?limit=${userInput}`).then((response) => {
        return response.json()
    }).then((data) => {
        data.forEach((indiData) => {

            if (indiData.category.includes(`clothing`)) {

                const productDiv = document.createElement('div')
                productDiv.innerHTML = `<div class='imageBox'><img class="productImages" src="${indiData.image}"/></div>
            <p class="productName">${indiData.title}</p>
            <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${indiData.rating['rate']}</button>
            <p class="productPrice">Price - ${indiData.price} $</p>
            <p class="productDescription">${indiData.description}</p>
            <button class="addToCart">Add To Cart</button>`
                productDiv.setAttribute('class', "miProducts")

                ClothesGrid.setAttribute('class', 'productsGrid')
                ClothesGrid.setAttribute('id', 'ClothesGrid')
                productDiv.setAttribute('class', "miProducts")
                document.getElementById("Clothing").appendChild(ClothesGrid);
                document.getElementById("ClothesGrid").appendChild(productDiv);
            }

            if (indiData.category === `jewelery`) {

                const productDiv = document.createElement('div')
                productDiv.innerHTML = `<div class='imageBox'><img class="productImages" src="${indiData.image}"/></div>
            <p class="productName">${indiData.title}</p>
            <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${indiData.rating['rate']}</button>
            <p class="productPrice">Price - ${indiData.price} $</p>
            <p class="productDescription">${indiData.description}</p>
            <button class="addToCart">Add To Cart</button>`
                productDiv.setAttribute('class', "miProducts")

                JwelleryGrid.setAttribute('class', 'productsGrid')
                JwelleryGrid.setAttribute('id', 'JwelleryGrid')
                productDiv.setAttribute('class', "miProducts")
                document.getElementById("Jwellery").appendChild(JwelleryGrid);
                document.getElementById("JwelleryGrid").appendChild(productDiv);
            }

            if (indiData.category === `electronics`) {

                const productDiv = document.createElement('div')
                productDiv.innerHTML = `<div class='imageBox'><img class="productImages" src="${indiData.image}"/></div>
            <p class="productName">${indiData.title}</p>
            <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${indiData.rating['rate']}</button>
            <p class="productPrice">Price - ${indiData.price} $</p>
            <p class="productDescription">${indiData.description}</p>
            <button class="addToCart">Add To Cart</button>`
                productDiv.setAttribute('class', "miProducts")

                EclectronicsGrid.setAttribute('class', 'productsGrid')
                EclectronicsGrid.setAttribute('id', 'EclectronicsGrid')
                productDiv.setAttribute('class', "miProducts")
                document.getElementById("Electronics").appendChild(EclectronicsGrid);
                document.getElementById("EclectronicsGrid").appendChild(productDiv);
            }


        })

    })
}
function getClothes() {
    fetch('https://fakestoreapi.com/products').then((response) => {
        return response.json()
    }).then((data) => {
        let ClothesGrid = document.createElement('div')
        let JwelleryGrid = document.createElement('div')
        let EclectronicsGrid = document.createElement('div')
        let OtherGrid = document.createElement('div')
        let productData = data
        data.forEach((indiData) => {
            if (indiData.category.includes(`clothing`)) {

                const productDiv = document.createElement('div')
                productDiv.innerHTML = `<div class='imageBox'><img onclick='getId(${indiData.id})' class="productImages" src="${indiData.image}"/></div>
        <p class="productName">${indiData.title}</p>
        <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${indiData.rating['rate']}</button>
        <p class="productPrice">Price - ${indiData.price} $</p>
        <p class="productDescription">${indiData.description}</p>
        <button class="addToCart">Add To Cart</button>`
                productDiv.setAttribute('class', "miProducts")

                ClothesGrid.setAttribute('class', 'productsGrid')
                ClothesGrid.setAttribute('id', 'ClothesGrid')
                productDiv.setAttribute('class', "miProducts")
                document.getElementById("Clothing").appendChild(ClothesGrid);
                document.getElementById("ClothesGrid").appendChild(productDiv);
            }

            if (indiData.category === `jewelery`) {

                const productDiv = document.createElement('div')
                productDiv.innerHTML = `<div class='imageBox'><img class="productImages" src="${indiData.image}"/></div>
        <p class="productName">${indiData.title}</p>
        <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${indiData.rating['rate']}</button>
        <p class="productPrice">Price - ${indiData.price} $</p>
        <p class="productDescription">${indiData.description}</p>
        <button class="addToCart">Add To Cart</button>`
                productDiv.setAttribute('class', "miProducts")

                JwelleryGrid.setAttribute('class', 'productsGrid')
                JwelleryGrid.setAttribute('id', 'JwelleryGrid')
                productDiv.setAttribute('class', "miProducts")
                document.getElementById("Jwellery").appendChild(JwelleryGrid);
                document.getElementById("JwelleryGrid").appendChild(productDiv);
            }

            if (indiData.category === `electronics`) {

                const productDiv = document.createElement('div')
                productDiv.innerHTML = `<div class='imageBox'><img class="productImages" src="${indiData.image}"/></div>
        <p class="productName">${indiData.title}</p>
        <button class="rating" onclick='buttonPress()'><img id='heartImage' style='height:1.5rem; width:2rem;padding-right:1rem' src='images/heart.png'/>${indiData.rating['rate']}</button>
        <p class="productPrice">Price - ${indiData.price} $</p>
        <p class="productDescription">${indiData.description}</p>
        <button class="addToCart">Add To Cart</button>`
                productDiv.setAttribute('class', "miProducts")

                EclectronicsGrid.setAttribute('class', 'productsGrid')
                EclectronicsGrid.setAttribute('id', 'EclectronicsGrid')
                productDiv.setAttribute('class', "miProducts")
                document.getElementById("Electronics").appendChild(EclectronicsGrid);
                document.getElementById("EclectronicsGrid").appendChild(productDiv);
            }


        })

    })

}
function backButton() {
    location.reload()
}